import 'dart:io';
import 'cards_service.dart';
import 'card.dart';

class Start {
  static List<Card> storage = [];
  static var uniques = -1;

  static void startProgram() {
    var userInput;

    print("""
    Choose an option by inserting a number:
    1. Read all cards
    2. Read card detail
    3. Search a card by a number
    4. Create a new card
    5. Update card
    6. Delete card
    """);

    do {
      userInput = stdin.readLineSync();
    } while (!['1', '2', '3', '4', '5', '6'].contains(userInput));
    switch (userInput) {
      case '1':
        CardsService.readAllCards(storage);
        break;
      case '2':
        CardsService.readCardDetail(storage);
        break;
      case '3':
        CardsService.search(storage);
        break;
      case '4':
        CardsService.createCard(storage, uniques);
        break;
      case '5':
        CardsService.updateCard(storage);
        break;
      case '6':
        CardsService.deleteCard(storage);
        break;
    }
  }
}
