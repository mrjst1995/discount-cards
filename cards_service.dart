import 'card.dart';
import 'dart:io';
import 'start.dart';
import 'input_validator.dart';

class CardsService {
  static int searchCardsByWholeNumber(List<Card> storage) {
    var cardIndex;
    print("Enter card code");
    var userInput = stdin.readLineSync();
    if (InputValidator.validateNumbersOnly(userInput)) {
      storage.forEach((element) {
        if (element.cardNumber.compareTo(userInput) == 0)
          cardIndex = storage.indexOf(element);
        else {
          print("Card doesn't exist");
        }
      });
    } else
      print("Card number must have 14 digits!");
    return cardIndex;
  }

  static String insertNameOrCity() {
    print("Press Yes to insert the value, or No to exit.");
    var userInputLocal = stdin.readLineSync();
    if (!['Yes', 'No'].contains(userInputLocal) == 0) {
      print("You must answer with Yes or No!");
      Start.startProgram();
      return null;
    }
    if (userInputLocal.compareTo('Yes') == 0) {
      print("Insert new value:");
      userInputLocal = stdin.readLineSync();
      if (InputValidator.validateLettersOnly(userInputLocal)) {
        return userInputLocal;
      } else {
        print("Value must contain only letters");
        Start.startProgram();
        return null;
      }
    } else {
      return null;
    }
  }

  static void createCard(List<Card> storage, var uniques) {
    var name;
    var city;
    var dateNow = DateTime.now();
    var year = dateNow.year + 1;
    var cardNumber;
    var userInput;

    print("""
          Insert a category digit:
          1-cosmetics
          2-books
          3-accessories
          4-services
          """);
    userInput = stdin.readLineSync();

    if (['1', '2', '3', '4'].contains(userInput)) {
      cardNumber = userInput;
    } else {
      print("Wrong input!");
      Start.startProgram();
    }

    print("Choose accumulation: Yes/No");
    userInput = stdin.readLineSync();
    if (['Yes', 'No'].contains(userInput)) {
      if (userInput.toString().compareTo('Yes') == 0)
        cardNumber += '1';
      else
        cardNumber += '0';
    }

    print("Choose discount percentage, choose between 5, 10, 20 and 30");
    userInput = stdin.readLineSync();
    if (['5', '10', '20', '30'].contains(userInput)) {
      if (userInput.compareTo('5') == 0) userInput = '05';
      cardNumber += userInput;
    } else {
      print("Wrong input");
      Start.startProgram();
    }

    if (cardNumber != null) {
      cardNumber += dateNow.day.toString() +
          dateNow.month.toString() +
          year.toString().substring(2);
    }

    if (uniques > 9999) {
      print("Unique number is out of range");
    } else if (uniques + 1 < 1000) {
      uniques++;
      var zeros = 4 - uniques.toString().length;
      for (int i = 0; i < zeros; i++) cardNumber += '0';
      cardNumber += uniques.toString();
    } else {
      uniques++;
      cardNumber += uniques.toString();
    }

    print("Insert name:");
    name = insertNameOrCity();

    print("Insert city:");
    city = insertNameOrCity();

    if (cardNumber != null && name != null && city != null) {
      storage.add(new Card(cardNumber, name, city));
    }
    Start.startProgram();
  }

  static void readAllCards(List<Card> storage) {
    storage.forEach((item) {
      print("Card code: " + item.cardNumber + " Customer's name: " + item.name);
    });
    Start.startProgram();
  }

  static void readCardDetail(List<Card> storage) {
    var cardIndex = searchCardsByWholeNumber(storage);
    if (cardIndex != null) {
      print("Card code: " +
          storage[cardIndex].cardNumber.toString() +
          "Customer's name: " +
          storage[cardIndex].name +
          "City: " +
          storage[cardIndex].city);
    }
    Start.startProgram();
  }

  static void deleteCard(List<Card> storage) {
    var cardIndex = searchCardsByWholeNumber(storage);
    if (cardIndex != null) storage.remove(storage[cardIndex]);
    Start.startProgram();
  }

  static void updateCard(List<Card> storage) {
    var isNull;
    var cardIndex = searchCardsByWholeNumber(storage);
    if (cardIndex != null) {
      print("Do you want to change name? Yes/No");
      isNull = insertNameOrCity();
      if (isNull != null) storage[cardIndex].name = isNull;
      print("Do you want to change city? Yes/No");
      isNull = insertNameOrCity();
      if (isNull != null) storage[cardIndex].city = isNull;
      Start.startProgram();
    }
  }

  static void search(List<Card> storage) {
    var userInput;

    print("Insert card code:");
    userInput = stdin.readLineSync();
    if (userInput.length > 14 || userInput.length < 3) {
      print("You've entered more than 14 digits or less than 3");
    } else {
      storage.forEach((element) {
        if (element.cardNumber.contains(userInput)) {
          print("Card code: " +
              element.cardNumber +
              "Name: " +
              element.name +
              "City: " +
              element.city);
        } else {
          print("Card doesn't exist!");
        }
      });
    }
    Start.startProgram();
  }
}
