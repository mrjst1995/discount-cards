class InputValidator {
  static bool validateLettersOnly(String input) {
    RegExp regexpLetters = RegExp(r'[a-zA-Z]+');

    if (regexpLetters.hasMatch(input) && input.isNotEmpty) {
      return true;
    } else
      return false;
  }

  static bool validateNumbersOnly(String input) {
    RegExp regexpNumbers = RegExp(r'\d{14}');

    if (regexpNumbers.hasMatch(input) && input.isNotEmpty) {
      return true;
    } else
      return false;
  }
}
